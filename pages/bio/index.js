import React from 'react'

import Firm from '../../components/Firm'
import Heading from '../../components/Heading'
import Layout from '../../components/Layout'

import useWindowDimensions from '../../hooks/useWindowDimensions'

import style from './bio.module.scss'

const contentful = require('contentful')

export async function getStaticProps() {
  const client = contentful.createClient({
    space: '932ewrgjdha8',
    accessToken: '7pZLEdaBZgcuWiR4Iya_uriwODcpfkZfjwucyqWFPmY',
  })

  const entries = await client.getEntries({
    content_type: 'firma',
  })

  return {
    props: {
      companies: entries.items,
    },
  }
}

export default function Bio({ companies }) {
  const { width } = useWindowDimensions()
  return (
    <Layout figure="bio" src="/images/ksztHome.png">
      <div className={style.container}>
        <Heading title="usprawnienia." titlewithMargin />
        {width < 768 && <img src="images/ksztHome.png" />}
        <section className={style.content}>
          <h3 className={style.subtitle}>kwalifikacje zawodowe</h3>
          <div>
            {companies.map((company, index) => {
              return <Firm companies={company} index={index} />
            })}
          </div>
        </section>
      </div>
      <img src="/images/ksztBio.png" className={style.bottomFigure} />
    </Layout>
  )
}
