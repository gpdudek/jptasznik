const path = require('path')
const { config } = require('process')

module.exports = {
  webpackFinal: async (baseConfig, options) => {
    // Modify or replace config. Mutating the original reference object can cause unexpected bugs.
    const { module = {} } = baseConfig

    const newConfig = {
      ...baseConfig,
      module: {
        ...module,
        rules: [...(module.rules || [])],
      },
    }

    // TypeScript with Next.js
    // newConfig.module.rules.push({
    //   test: /\.(js)$/,
    //   include: [path.resolve(__dirname, '../components'), path.resolve(__dirname, '../stories')],
    //   use: [
    //     {
    //       loader: 'babel-loader',
    //       options: {
    //         presets: ['next/babel'],
    //         plugins: ['react-docgen'],
    //       },
    //     },
    //   ],
    // })
    // newConfig.resolve.extensions.push('.js')

    //
    // CSS Modules
    // Many thanks to https://github.com/storybookjs/storybook/issues/6055#issuecomment-521046352
    //

    return newConfig
  },
}
