import React from 'react'
import clsx from 'clsx'

import Navbar from '../Navbar'
import Footer from '../Footer'

import homeStyle from '../../styles/Home.module.scss'

import style from './layout.module.scss'

export default function Layout({ children, figure, src }) {
  return (
    <>
      <Navbar />
      <div className={style.content}>
        <div className={style.containerChildren}>{children}</div>
        {src && (
          <img
            className={clsx(
              figure === 'home' && style.homeFigure,
              figure === 'bio' && style.bioFigure,
              figure === 'projekty' && style.projektyFigure,
            )}
            src={src}
          />
        )}
      </div>
      <Footer />
    </>
  )
}
