import React from 'react'

import Button from './index'

export default {
  title: 'Components/Button',
  component: Button,
}

export const withText = () => <Button>Button</Button>
export const withIcon = () => <Button>😄 👍 😍 🤓</Button>

// const Template = (args) => <Button {...args} />

// export const withText = Template.bind({})
// withText.args = { children: 'wyslij' }

// export const withIcon = Template.bind({})
// withIcon.args = { ...withText.args, children: '😄👍😍💯' }

// export const withIcon = Template.bind({})
// withIcon.args = { ...withText.args, children: '📚📕📈🤓' }
