import React from 'react'
import { render } from '@testing-library/react'
import Form from './index'

describe('Input component', () => {
  it('renders input element', () => {
    const { getByPlaceholderText } = render(<Form />)

    expect(getByPlaceholderText(/email/i)).toBeInTheDocument()
  })
})
