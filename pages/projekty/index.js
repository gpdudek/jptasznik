import React from 'react'

import Blockslist from '../../components/Blockslist'
import Layout from '../../components/Layout'

import useWindowDimensions from '../../hooks/useWindowDimensions'

import style from './projekty.module.scss'

const contentful = require('contentful')

export async function getStaticProps() {
  const client = contentful.createClient({
    space: '932ewrgjdha8',
    accessToken: '7pZLEdaBZgcuWiR4Iya_uriwODcpfkZfjwucyqWFPmY',
  })

  const entries = await client.getEntries({
    content_type: 'project',
  })

  return {
    props: {
      projects: entries.items,
    },
  }
}

export default function Projekty({ projects }) {
  const { width } = useWindowDimensions()
  return (
    <Layout figure="projekty" src="images/kszProjekty.png">
      <section className={style.container}>
        <h1 className={style.heading}>Jędrzej STUDIO.</h1>
        <p className={style.desc}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nunc risus, tincidunt
          sit amet aliquam quis.
        </p>
      </section>
      {width < 768 && <img src="images/kszProjekty.png" />}
      <section className={style.blockContainer}>
        <Blockslist projects={projects} />
      </section>
    </Layout>
  )
}
