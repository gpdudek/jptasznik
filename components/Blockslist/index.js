import React from 'react'

import Block from '../Block'
import useWindowDimensions from '../../hooks/useWindowDimensions'

import style from './blockslist.module.scss'

export default function Blockslist({ projects = [] }) {
  // const getBlockItems = () => {
  //   return [
  //     {
  //       src: '/images/esther-jiao.png',
  //       title: 'skala',
  //       text:
  //         'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nunc risus, tincidunt sit amet aliquam quis, auctor quis enim. Aliquam ut orci sit amet tortor sagittis eleifend. Nulla facilisi. Quisque ligula diam, egestas vel justo in, pretium laoreet enim. Donec commodo quis dui vitae volutpat. Nunc velit nisi, varius at tempus nec, sollicitudin quis ex. ',
  //     },
  //     {
  //       src: '/images/joel-filipe-unsplash.png',
  //       title: 'struktura',
  //       text:
  //         'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nunc risus, tincidunt sit amet aliquam quis, auctor quis enim. Aliquam ut orci sit amet tortor sagittis eleifend. Nulla facilisi. Quisque ligula diam, egestas vel justo in, pretium laoreet enim. Donec commodo quis dui vitae volutpat. Nunc velit nisi, varius at tempus nec, sollicitudin quis ex. ',
  //     },
  //     {
  //       src: '/images/joel-filipe-unsplash.png',
  //       title: 'bryła',
  //       text:
  //         'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nunc risus, tincidunt sit amet aliquam quis, auctor quis enim. Aliquam ut orci sit amet tortor sagittis eleifend. Nulla facilisi. Quisque ligula diam, egestas vel justo in, pretium laoreet enim. Donec commodo quis dui vitae volutpat. Nunc velit nisi, varius at tempus nec, sollicitudin quis ex. ',
  //     },
  //     {
  //       src: '/images/ricardo-gomez-angel-0rgv6FdK9hs-unsplash.png',
  //       title: 'forma',
  //       text:
  //         'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nunc risus, tincidunt sit amet aliquam quis, auctor quis enim. Aliquam ut orci sit amet tortor sagittis eleifend. Nulla facilisi. Quisque ligula diam, egestas vel justo in, pretium laoreet enim. Donec commodo quis dui vitae volutpat. Nunc velit nisi, varius at tempus nec, sollicitudin quis ex. ',
  //     },
  //     {
  //       src: '/images/ricardo-gomez-angel-0rgv6FdK9hs-unsplash.png',
  //       title: 'forma',
  //       text:
  //         'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nunc risus, tincidunt sit amet aliquam quis, auctor quis enim. Aliquam ut orci sit amet tortor sagittis eleifend. Nulla facilisi. Quisque ligula diam, egestas vel justo in, pretium laoreet enim. Donec commodo quis dui vitae volutpat. Nunc velit nisi, varius at tempus nec, sollicitudin quis ex. ',
  //     },
  //     {
  //       src: '/images/joel-filipe-unsplash.png',
  //       title: 'skała',
  //       text:
  //         'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nunc risus, tincidunt sit amet aliquam quis, auctor quis enim. Aliquam ut orci sit amet tortor sagittis eleifend. Nulla facilisi. Quisque ligula diam, egestas vel justo in, pretium laoreet enim. Donec commodo quis dui vitae volutpat. Nunc velit nisi, varius at tempus nec, sollicitudin quis ex. ',
  //     },
  //     {
  //       src: '/images/esther-jiao.png',
  //       title: 'bryła',
  //       text:
  //         'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nunc risus, tincidunt sit amet aliquam quis, auctor quis enim. Aliquam ut orci sit amet tortor sagittis eleifend. Nulla facilisi. Quisque ligula diam, egestas vel justo in, pretium laoreet enim. Donec commodo quis dui vitae volutpat. Nunc velit nisi, varius at tempus nec, sollicitudin quis ex. ',
  //     },
  //   ]
  // }

  const { width } = useWindowDimensions()

  return (
    <ul className={style.container}>
      {projects.map((project, index) => {
        return (
          <li className={style.projectBlock} key={index}>
            <div className={style.firstHorizontalLine} />
            {width < 768 ? (
              <Block key={index} src={item.src} title={item.title} text={item.text} />
            ) : (
              <Block
                key={index}
                src={project.fields.picture.fields.file.url}
                title={project.fields.title}
                text={project.fields.description}
                crossOut={index === 2 || index === 6}
                verticalLine={index === 1 || index === 5}
              />
            )}
            <div className={style.secondHorizontalLine} />
          </li>
        )
      })}
    </ul>
  )
}
