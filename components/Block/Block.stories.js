import React from 'react'

import pic from '../../public/images/joel-filipe-unsplash.png'

import Block from './index'

export default {
  title: 'Components/Block/Block',
  component: Block,
}

export const standard = (arg) => (
  <Block
    src={pic}
    title="skala"
    text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nunc risus, tincidunt sit amet aliquam quis, auctor quis enim. Aliquam ut orci sit amet tortor sagittis eleifend. Nulla facilisi. Quisque ligula diam, egestas vel justo in, pretium laoreet enim. Donec commodo quis dui vitae volutpat. Nunc velit nisi, varius at tempus nec, sollicitudin quis ex. "
  />
)
