import { createClient } from 'contentful'

class CMS {
  constructor() {
    this.client = createClient({
      space: 'gw21y7tc3txy',
      accessToken: 'a15754d5b59f1841321f03e97a1f8d2cb46e6041fb79bd7559db9fecd9d5a374',
    })
  }

  getList(type, page = 1, perPage = 20, orderBy = null, options = {}) {
    return this.client
      .getEntries({
        content_type: type,
        include: 3,
        order: orderBy,
        limit: perPage,
        skip: Math.max(page - 1, 0) * perPage,
        ...options,
      })
      .then((result) => {
        result.perPage = perPage
        result.currentPage = page
        result.totalPages = Math.ceil(result.total / perPage)
        return result
      })
  }

  getAll(type) {
    return this.client
      .getEntries({
        content_type: type,
        include: 3,
      })
      .then((result) => result.items)
  }

  getEntry(id) {
    return this.client
      .getEntries({
        'sys.id': id,
        include: 3,
      })
      .then((result) => result.items[0])
  }

  getAsset(id) {
    return this.client.getAsset(id)
  }

  getHeroPhotos() {
    return this.getAll('heroPhoto')
  }

  getBio() {
    return this.getEntry('3MRITxq9pYwYYIumSQGGOW')
  }

  getConcerts(page = 1, past = false) {
    return this.getList('concert', 1, 999, 'fields.date')
  }

  getNews() {
    return this.getList('news', 1, 20, '-fields.date')
  }

  getNewsItem(id) {
    return this.getEntry(id)
  }

  getPhotos() {
    return this.getList('photo', 1, 999)
  }

  getMedia() {
    return this.getList('media', 1, 999)
  }

  getContacts() {
    return this.getList('contact', 1, 999, 'fields.displayPosition')
  }
}

const cms = new CMS()

export default cms
