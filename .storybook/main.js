module.exports = {
  stories: ['../components/**/*.stories.js'],
  // stories: ['../src/**/(*.stories|stories).(ts|tsx|js|mdx)'],
  addons: [
    {
      name: '@storybook/preset-scss',
      options: {
        cssLoaderOptions: {
          modules: true,
        },
      },
    },
    // You can add other presets/addons by using the string declaration
    // '@storybook/addon-actions',
  ],
}
