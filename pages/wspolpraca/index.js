import React from 'react'

import Form from '../../components/Form'
import Heading from '../../components/Heading'
import Layout from '../../components/Layout'

import style from './wspolpraca.module.scss'

export default function Wspolpraca() {
  return (
    <Layout>
      <div className={style.diagonal} />
      <Heading title="zostańmy w kontakcie." />
      <Form />
    </Layout>
  )
}
