import Head from 'next/head'

import '../styles/globals.scss'
import '../styles/utils.scss'

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <title>Jędrzej STUDIO.</title>
      </Head>
      <Component {...pageProps} />
    </>
  )
}

export default MyApp
