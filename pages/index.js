import React from 'react'

import Button from '../components/Button'
import Blockslist from '../components/Blockslist'
import Form from '../components/Form'
import Layout from '../components/Layout'

import useWindowDimensions from '../hooks/useWindowDimensions'

import style from './index.module.scss'

const contentful = require('contentful')

export async function getStaticProps() {
  const client = contentful.createClient({
    space: '932ewrgjdha8',
    accessToken: '7pZLEdaBZgcuWiR4Iya_uriwODcpfkZfjwucyqWFPmY',
  })

  const entries = await client.getEntries({
    content_type: 'project',
  })

  return {
    props: {
      projects: entries.items,
    },
  }
}

export default function Home({ projects = [] }) {
  const { width } = useWindowDimensions()
  return (
    <Layout figure="home" src="/images/ksztHome.png">
      <section className={style.hero}>
        <h1 className={style.heading}>Jędrzej STUDIO.</h1>
        <p className={style.paragraph}>
          Jestem projektantem, bo lubię obserwować twórczy potencjał uczniów, dawać im drobne
          wskazówki i swobodę twórczą.
        </p>
        <Button>kontakt</Button>
        {width < 768 && <img src="images/ksztHome.png" className={style.image} />}
      </section>
      <section>
        <Blockslist projects={projects} />
      </section>
      <section>
        <Form heading />
      </section>
    </Layout>
  )
}
