import React from 'react'

import Navbar from './index'

export default {
  title: 'Components/Navbar',
  component: Navbar,
}

export const navbar = () => <Navbar />
