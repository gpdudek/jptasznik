import React from 'react'
import style from './heading.module.scss'

export default function Heading({ title, titlewithMargin }) {
  return (
    <section className={style.container}>
      <h2 className={titlewithMargin ? style.titleWithMargin : style.title}>{title}</h2>
    </section>
  )
}
