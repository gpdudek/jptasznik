import React from 'react'

import Link from 'next/link'
import { useState, useRef } from 'react'
import cx from 'classnames'
import VizSensor from 'react-visibility-sensor'

import useWindowDimensions from '../../hooks/useWindowDimensions'
import useDetectOutsideClick from '../../hooks/useDetectOutsideClick'

import style from './navbar.module.scss'

export default function Navbar() {
  const [isOpenMenu, setCloseMenu] = useState(false)

  return (
    <nav className={style.container} data-testid="navbar">
      <ul className={style.content}>
        <li className={style.item}>
          <Link href="/">
            <a href="/" data-testid="link-home">
              home
            </a>
          </Link>
        </li>
        <li className={style.item}>
          <Link href="/bio">
            <a href="/bio">bio</a>
          </Link>
        </li>
        <li className={style.item}>
          <Link href="/wspolpraca">
            <a href="/wspolpraca">współpraca</a>
          </Link>
        </li>
        <li className={style.item}>
          <Link href="/projekty">
            <a href="/projekty">projekty</a>
          </Link>
        </li>
      </ul>
    </nav>
  )
}
