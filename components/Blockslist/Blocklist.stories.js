import React from 'react'

import BlockList from './index'

export default {
  title: 'Components/Block/BlockList',
  component: BlockList,
}

export const standard = () => <BlockList />
