import React from 'react'

import style from './footer.module.scss'

export default function Footer() {
  return (
    <div className={style.container}>
      <h1 className={style.heading}>Jędrzej STUDIO.</h1>
      <img src="icons/linkedin-brands.png" className={style.logo} />
      <p className={style.copywriter}>© 2020</p>
    </div>
  )
}
