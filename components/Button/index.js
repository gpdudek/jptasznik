import React from 'react'
import clsx from 'clsx'

import style from './button.module.scss'

export default function Button({ children, className, ...otherprops }) {
  return (
    <button className={clsx(style.whiteButton, className)} {...otherprops}>
      {children}
    </button>
  )
}
