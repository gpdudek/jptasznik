import React from 'react'
import { render } from '@testing-library/react'
import Navbar from './index'

describe('Navbar component', () => {
  it('renders navbar element', () => {
    const { getByTestId } = render(<Navbar />)

    expect(getByTestId('navbar')).toBeInTheDocument()
  })
  it('renders link home ', () => {
    const { getByTestId } = render(<Navbar />)

    expect(getByTestId('link-home')).toBeInTheDocument()
    expect(getByTestId('link-home').textContent).toEqual('home')
  })
})
