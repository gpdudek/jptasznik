import React from 'react'

import style from './firm.module.scss'

export default function Firm({ companies = [] }) {
  console.log('companies', companies)
  return (
    <div className={style.container}>
      <div>
        <h1 className={style.firm}>{companies.fields.companyName}</h1>
        <p className={style.desc}>{companies.fields.responsibilities}</p>
      </div>
      <p className={style.desc}>{companies.fields.description}</p>
    </div>
  )
}
