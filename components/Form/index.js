import React from 'react'
import { useFormik } from 'formik'

import Button from '../Button'
import useWindowDimensions from '../../hooks/useWindowDimensions'

import style from './form.module.scss'

const validate = (values) => {
  const errors = {}
  if (!values.name) {
    errors.name = 'Required'
  } else if (values.name.length > 25) {
    errors.name = 'Must be 25 characters or less'
  }

  if (!values.firm) {
    errors.firm = 'Required'
  } else if (values.firm.length > 5) {
    errors.firm = 'Must be 5 characters or less'
  }

  if (!values.email) {
    errors.email = 'Required'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address'
  }

  if (!values.help) {
    errors.help = 'Required'
  } else if (values.pomoc.length > 20) {
    errors.help = 'Must be 20 characters or less'
  }

  return errors
}

export default function Form({ heading = false }) {
  // Pass the useFormik() hook initial form values and a submit function that will
  // be called when the form is submitted
  const formik = useFormik({
    initialValues: {
      name: '',
      firm: '',
      email: '',
      help: '',
    },
    validate,
    onSubmit: (values) => {
      alert(JSON.stringify(values, null, 2))
    },
  })

  const { width } = useWindowDimensions()

  return (
    <div>
      {heading && <h2 className={style.heading}>zostańmy w kontakcie</h2>}
      <div className={width > 768 && style.contentFormBackground}>
        <div className={style.containerForm}>
          <p className={style.headingDesc}>
            JESTEM OTWARTY NA NOWE WYZWANIA I Z PRZYJEMNOŚCIĄ POZNAM PROJEKT, NAD KTÓRYM CHCESZ
            PRACOWAĆ
          </p>

          <form onSubmit={formik.handleSubmit} className={style.contentForm} heading>
            <div className={style.inputs}>
              <input
                className={style.item}
                name="email"
                type="email"
                placeholder="email"
                onChange={formik.handleChange}
                value={formik.values.email}
              />
              {formik.errors.email ? (
                <div className={style.error}>{formik.errors.email}</div>
              ) : null}
              <input
                className={style.item}
                name="Name"
                type="text"
                placeholder="Imię i nazwisko"
                onChange={formik.handleChange}
                value={formik.values.name}
              />
              {formik.errors.name ? <div className={style.error}>{formik.errors.name}</div> : null}

              <input
                className={style.item}
                name="firm"
                type="text"
                placeholder="firma"
                onChange={formik.handleChange}
                value={formik.values.firm}
              />
              {formik.errors.firm ? <div className={style.error}>{formik.errors.firm}</div> : null}

              <input
                className={style.item}
                name="help"
                type="textarea"
                component="textarea"
                rows="2"
                placeholder="W czym mogę pomóc?"
                onChange={formik.handleChange}
                value={formik.values.help}
              />
              {formik.errors.help ? <div className={style.error}>{formik.errors.help}</div> : null}
            </div>
            <Button type="submit" className={style.buttonSend}>
              Wyślij
            </Button>
            {width <= 768 && (
              <img src="/images/ksztHomeContact.png" className={style.mobileFigure} />
            )}
          </form>
        </div>
      </div>
    </div>
  )
}
