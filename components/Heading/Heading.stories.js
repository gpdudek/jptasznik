import React from 'react'

import Heading from './index'

export default {
  title: 'Components/Heading',
  component: Heading,
}

export const main = (title) => <Heading title="uprawnienia." />
