import React from 'react'
import clsx from 'clsx'

import style from './block.module.scss'

export default function Block({ src, className, title, text, crossOut, verticalLine }) {
  return (
    <div className={clsx(style.container, className)}>
      <h1 className={style.title}>{title}</h1>
      {crossOut && <div className={style.crossOutLine} />}
      <div className={verticalLine ? style.contentVerticalLine : style.content}>
        <div className={style.imgWrapper}>
          <img src={src} className={style.img} />
          {verticalLine && <div className={style.verticalLine} />}
        </div>
        <p className={style.text}>{text}</p>
      </div>
    </div>
  )
}
